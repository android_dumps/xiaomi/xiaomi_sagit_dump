#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:22730062:3161254601982d849e9c87cdcaa9b57bc1af018c; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:19146058:ad9360db8bbf364c51f9ed235364e3c59d0fc7cf EMMC:/dev/block/bootdevice/by-name/recovery 3161254601982d849e9c87cdcaa9b57bc1af018c 22730062 ad9360db8bbf364c51f9ed235364e3c59d0fc7cf:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
